#include <fstream>
#include "rabin-karp.h"
#include <string>

int main()
{
	ifstream f;
	ofstream g;

	f.open("input.txt");
	g.open("results.txt");

	string textaux = "";
	string patternaux = "";	

	time_t t1;
	time_t t2;


	for (int i = 1; i <= 3; i++) {

		getline(f, patternaux);
		getline(f, textaux);

		const char* text = textaux.c_str();
		const char* pattern = patternaux.c_str();
		g << endl << "Testul " << i << endl << endl;

		t1 = clock();
		g << "Numar de apartii folosind Rabin-Karp : " << substring_count(text, pattern) << endl;
		g << "Prima aparitie folosind Rabin-Karp : " << substring_search(text, pattern) << endl << endl;
		t1 = clock() - t1;

		t2 = clock();
		g << "Numar de aparitii folosind burte-force : " << substring_count_brute_force(text, pattern) << endl;
		g << "Prima aparitie folosind burte-force : " << substring_search_brute_force(text, pattern) << endl << endl;
		t2 = clock() - t2;

		g << "Timp utilizand Rabin-Karp : " << (float)t1 / CLOCKS_PER_SEC << " clocks" << endl;
		g << "Timp utilizand brute-force : " << (float)t2 / CLOCKS_PER_SEC << " clocks" << endl << endl;
	}
	
	return 0;
}
