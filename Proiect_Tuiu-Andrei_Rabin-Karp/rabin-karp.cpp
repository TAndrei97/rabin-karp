#include "rabin-karp.h"

long prime_powerd;

char* substr(const char* source, int start) {
	char* aux = "";
	strcpy(aux, source + start);
	return aux;
}

int substring_search(const char* text, const char* pattern) {

	int textLen = strlen(text);
	int patternLen = strlen(pattern);
	int i;

	long patternH = h(pattern, patternLen);
	long textH = h(text, patternLen);

	for (i = 0; i <= textLen - patternLen; i++) {

		if (textH == patternH && checkEquals(text, i, pattern)) 
			return i;

		textH = recalculate_h(text, textH, i, patternLen, prime_powerd);
	}

	return -1;
}

int substring_count(const char* text, const char* pattern, int start) {

	int count = 0;
	char* aux = new char[strlen(text)];

	strcpy(aux, text + start);

	while (substring_search(aux, pattern) != -1) {
		start = substring_search(aux, pattern);
		strcpy(aux, aux + (start + 1));
		count++;	
	}

	return count;
}


bool checkEquals(const char* text, int textIndex, const char* pattern) {

	int i;
	int len = strlen(pattern);

	for (i = 0; i < len; i++) {
		if (pattern[i] != text[i + textIndex])
			return false;
	}

	return true;
}

long h(const char* text, int length, int prime, int d) {

	long result = 0;

	for (int i = 0; i < length; i++) {
		result = (result * d + text[i]) % prime;
	}

	prime_powerd = 1;

	for (int i = 0; i < length - 1; i++) {
		prime_powerd = (prime_powerd * d) % prime;
	}

	return result;
}

long recalculate_h(const char* text, long oldHash, int oldIndex, int patternLen, long p, int prime, int d) {

	oldHash = (d * (oldHash - text[oldIndex] * p) + text[oldIndex + patternLen]) % prime;

	if (oldHash < 0)
		return oldHash + prime;

	return oldHash;

}

int substring_search_brute_force(const char* text, const char* pattern) {

	int i;
	int j;
	bool find = false;

	for (i = 0; i < strlen(text); i++) {
		
		find = true;

		for (j = 0; j < strlen(pattern); j++) {
			if (pattern[j] != text[j + i]) {
				find = false;
				break;
			}

		}

		if (find)
			return i;
	}

	return -1;
}

int substring_count_brute_force(const char* text, const char* pattern, int start) {

	int count = 0;
	char* aux = new char[strlen(text)];

	strcpy(aux, text + start);

	while (substring_search(aux, pattern) != -1) {
		start = substring_search_brute_force(aux, pattern);
		strcpy(aux, aux + (start + 1));
		count++;
	}

	return count;
}
