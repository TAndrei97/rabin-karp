#pragma once

#include <iostream>	
#include <string>
#include <functional>

using namespace std;

int hash(string text, int base = 256, int prime = 101);
int recalculate_hash(string text, int oldHash, int base = 256, int prime = 101);
int substring_search1(string text, string pattern);
bool checkEquals(string text, string pattern);
int substring_count1(string text, string pattern, int start = 0);