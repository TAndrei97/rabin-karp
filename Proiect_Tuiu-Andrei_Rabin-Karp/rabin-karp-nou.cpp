#include "rabin-karp-nou.h"

//int hash(string text, int base, int prime) {
//
//}

int substring_search1(string text, string pattern) {
	int patternLen = pattern.length();
	int textLen = text.length();
	size_t patternH = hash<string>{}(pattern);
	size_t text_h;
	//= hash<string>{}(text.substr(0, patternLen - 1));
	//cout << patternH;

	int i;

	for (i = 0; i <= textLen - patternLen; i++) {
		text_h = hash<string>{}(text.substr(i, patternLen));
		if (text_h == patternH && checkEquals(text.substr(i, patternLen), pattern)) {
			return i;
		}
	}
	return -1;


}

bool checkEquals(string text, string pattern) {

	int i;
	int len = pattern.size();

	for (i = 0; i < len; i++) {

		if (pattern[i] != text[i])
			return false;

	}

	return true;

}

int substring_count1(string text, string pattern, int start) {

	int count = 0;
	text = text.substr(start);

	while (substring_search1(text, pattern) != -1) {

		start = substring_search1(text, pattern);
		text = text.substr(start + 1);
		count++;

	}

	return count;

}

int hash(string text, int base, int prime) {
	int textLen = text.length();
	int textH = 0;
	for (int i = 0; i < textLen; i++) {
		textH = (base * textH + text[i]) % prime;
	}
	return textH;
}