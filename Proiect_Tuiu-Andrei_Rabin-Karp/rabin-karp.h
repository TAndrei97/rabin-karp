#pragma once

#include <iostream>
#include <time.h>
#include <string.h>

using namespace std;

char* substr(const char* source, int start);
int substring_count_brute_force(const char* text, const char* pattern, const int start = 0);
int substring_search_brute_force(const char* text, const char* pattern);
bool checkEquals(const char* text, int textIndex, const char* pattern);
int substring_search(const char* text, const char* pattern);
int substring_count(const char* text, const char* pattern, int start = 0);
long h(const char* text, int length, int prime = 101, int d = 256);
long recalculate_h(const char* text, long oldHash, int oldIndex, int patternLen, long p, int prime = 101, int d = 256);